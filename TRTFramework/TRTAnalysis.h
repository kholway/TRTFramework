#ifndef TRTFramework_TRTAnalysis_H
#define TRTFramework_TRTAnalysis_H

#include "TRTFramework/TRTIncludes.h"
#include "TRTFramework/Config.h"

#include "TRTFramework/TRTUtils.h"
#include "TRTFramework/HistogramStore.h"
#include "TRTFramework/LepHandler.h"

class TRTAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
private:
  // float cutValue;
  std::set<unsigned long long int> runEvtList;
  TRT::Config m_config; 
  
  // the xAOD event reader
  xAOD::TEvent *m_event;  //!
  xAOD::TStore *m_store; //!
  
  HistogramStore *m_histoStore; //!
  TRT::LepHandler *m_lepHandler; //!
  
  CP::IPileupReweightingTool *m_PRWTool; //!
  GoodRunsListSelectionTool *m_grl; //!
  
  int m_eventCounter; //!
  long m_startTime; //!

  double m_prwSF;
  bool m_usePRW;
  bool m_removeOverlap;

  double m_WeightAll;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

public:

  // this is a standard constructor
  TRTAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();
  
  virtual EL::StatusCode createOutput();
  
  inline virtual void setConfig(const TRT::Config &config) { m_config = config; }

  // this is needed to distribute the algorithm to the workers
  ClassDef(TRTAnalysis, 1);

protected:

  // access to useful pointers
  inline virtual xAOD::TEvent*      event() { return m_event; }
  inline virtual xAOD::TStore*      store() { return m_store; }
  inline virtual const xAOD::EventInfo*  eventInfo(); 
  
  inline virtual HistogramStore*       histoStore() { return m_histoStore; }
  inline virtual TRT::Config*              config() { return &m_config; }
  inline virtual TRT::LepHandler*      lepHandler() { return m_lepHandler; }
 
// TRTVariables
protected:

  double getTrackOccupancy( const xAOD::TrackParticle *track );
  
  inline double weight() { return m_WeightAll; }
  double mcWeight();
  double pileupWeight();
  double averageMu();  

  int NTracks();
  int numberOfPrimaryVertices();
  int NPV() { return numberOfPrimaryVertices(); }
  
  bool passGRL();
  bool isDuplicate();
  bool hasZeroWeight();
  
  inline bool isMC();
  inline bool isData();

};

inline
const xAOD::EventInfo* TRTAnalysis::eventInfo()
{
  const xAOD::EventInfo *eventinfo = 0;
  if (m_event->retrieve(eventinfo, "EventInfo").isFailure()) {
    TRT::fatal("Cannot access EventInfo");
  }
  return eventinfo;
}

inline
bool TRTAnalysis::isMC()
{
  static bool isMC = eventInfo()->eventType(xAOD::EventInfo::IS_SIMULATION);
  return isMC;
}

inline
bool TRTAnalysis::isData()
{
  static bool isData = not eventInfo()->eventType(xAOD::EventInfo::IS_SIMULATION);
  return isData;
}


#endif
