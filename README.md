Author: jared.vasquez@yale.edu

***

For first time users, try our [**Getting Started**](https://gitlab.cern.ch/jvasquez/TRTFramework/wikis/GettingStarted) guide.

Extensive documentation and guides can be found in our [wiki](https://gitlab.cern.ch/jvasquez/TRTFramework/wikis/home)

