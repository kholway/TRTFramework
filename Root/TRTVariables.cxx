#include <TRTFramework/TRTAnalysis.h>
#include "TRTFramework/TRTIncludes.h"


bool TRTAnalysis::passGRL()
//------------------------------------------------------------------------------
{
  if (isData() && not m_grl->passRunLB(*eventInfo())) return false;
  return true;
}


bool TRTAnalysis::isDuplicate()
//------------------------------------------------------------------------------
{
  if (m_removeOverlap) {
    int event = eventInfo()->eventNumber();
    int run = eventInfo()->runNumber();
    if (isMC()) run = eventInfo()->mcChannelNumber();
    unsigned long long int tag = 100000000000*run + event;
    if ( runEvtList.count(tag) == 0 ) runEvtList.insert( tag );
    else return true;
  }
  return false;
}


bool TRTAnalysis::hasZeroWeight() 
//------------------------------------------------------------------------------
{
  return ( m_usePRW && m_WeightAll == 0.0 );
}


int TRTAnalysis::numberOfPrimaryVertices()
//------------------------------------------------------------------------------
{
  const xAOD::VertexContainer *vertices = nullptr;
  if (m_event->retrieve(vertices, "PrimaryVertices").isFailure())
    TRT::fatal("Cannot access PrimaryVertices");

  int NPV = 0;
  for (auto vertex: *vertices) {
    if (vertex->vertexType() == xAOD::VxType::PriVtx ||  vertex->vertexType() == xAOD::VxType::PileUp) NPV++;
  }

  return NPV;
}


double TRTAnalysis::averageMu()
//------------------------------------------------------------------------------
{
  double mu = eventInfo()->averageInteractionsPerCrossing();
  if (isData() && m_usePRW)
    mu = m_PRWTool->getCorrectedMu(*eventInfo())*m_prwSF;  
  return mu;
}


double TRTAnalysis::mcWeight()
//------------------------------------------------------------------------------
{
  if( isMC() ) {
    double mcweight = 1.0;
    const std::vector<float> weights = eventInfo()->mcEventWeights();
    if (weights.size() > 0) mcweight = weights[0];
    return mcweight;
  }

  return 1.0;
}


double TRTAnalysis::pileupWeight()
//------------------------------------------------------------------------------
{
  double pileupWeight(1.0);
  if( isMC() && m_usePRW ) {
    //CHECK( m_PRWTool->apply(*eventInfo()) );
    pileupWeight = m_PRWTool->getCombinedWeight(*eventInfo());
  }
  return pileupWeight;
}


double TRTAnalysis::getTrackOccupancy( const xAOD::TrackParticle *track )
//------------------------------------------------------------------------------
{
  // Check for 'TRTLocalOccupancy' definition
  if (track->isAvailable<float>("TRTLocalOccupancy"))
    return track->auxdata<float>("TRTLocalOccupancy");

  // else assume 'TRTTrackOccupancy' definition
  return track->auxdata<float>("TRTTrackOccupancy");
}


int TRTAnalysis::NTracks()
//------------------------------------------------------------------------------
{
  const xAOD::TrackParticleContainer* tracks; 
  if (not m_event->retrieve( tracks, "InDetTrackParticles" ).isSuccess() ){
    TRT::fatal("Could not get InDetTrackParticles collection");
  }

  int NTracks(0);
  for( auto track : *tracks ) {
    if (lepHandler()->passSelection(track)) NTracks++;
  }

  return NTracks;
}
